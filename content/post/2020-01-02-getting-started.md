---
title: Getting Started
subtitle: About the first app
date: 2020-01-02
tags: ["AWS", "Amplify"]
---

Where to get started ...  

Lets build a photo sharing app using AWS and their Amplify framework.

## Prerequisites

- AWS CLI
- AWS IAM user
- NodeJS
- jq

Example commands:
```bash
# Update the AWS CLI
pip install --user --upgrade awscli

# Install the AWS Amplify CLI
npm install -g @aws-amplify/cli
amplify configure

# Install jq (after downloading release binary)
cp ~/Downloads/jq-osx-amd64 /usr/local/bin/jq
chmod +x /usr/local/bin/jq
```

Note the profile created during amplify configure as it will be necessary when we initialise amplify for the Angular project later.

You can verify that you are running at least Node.js version 8.12 or greater and npm version 5.x or greater by executing:
```bash
node -v
npm -v
```

## Basic Architecture

![Photo Sharing App Architecture Overview](/images/PhotoShareApp.png)

We’ll use a number of AWS services and tools in our solution, including:
- The AWS Amplify CLI, to rapidly provision and configure our cloud services
- The AWS Amplify JavaScript library, to connect our front end to cloud resources
- Amazon Cognito, to handle user sign up authorization
- Amazon Simple Storage Service (S3), to store and serve as many photos as our users care to upload, and to host the static assets for our app
- Amazon DynamoDB, to provide millisecond response times to API queries for album and photo data
- AWS AppSync, to host a GraphQL API for our front end
- AWS Lambda, to create photo thumbnails asynchronously in the cloud
- Amazon Rekognition, to detect relevant labels for uploaded photos
- Amazon Elasticsearch Service, to index and search our photos by their labels
