---
title: "Initialise the Angular app"
date: 2020-01-03
tags: ["Angular", "AWS", "Amplify"]
---

The front-end will be developed using Angular. So first ensure that Angular is installed

Example commands:
```bash
# Install the Angular CLI
npm install -g @angular/cli
```

You can verify the version of Angular that you are running by executing:
```bash
ng version
```

As we already created a project shell on GitLab we will generate the Angular project from the relevant folder with the following command:
```bash
ng new share-my-trip-app --routing=true --style=scss --skip-git --directory=./ --prefix=share-my-trip-app
```

Add material design to the default Angular App
```bash
ng add @angular/material
```
