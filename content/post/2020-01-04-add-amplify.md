---
title: "Extend the Angular App with the Amplify framework"
date: 2020-01-04
tags: ["Angular", "AWS", "Amplify"]
---

Now that there is a basic Angular app with Material design, lets extend the application with basic Amplify configuration.

First ensure that the necessary prerequisites are installed for the application:
```bash
npm install aws-amplify aws-amplify-angular
```

Then we need to initialise Amplify from the root of the app directory
```bash
amplify init
```

Choose the following values:
- Default project name (press enter)
- *master* for environment name
- *Visual Studio Code* for default editor
- *javascript* for type of app
- *angular* for javascript framework
- *src* for source directory
- *dist/angular-app-name* for distribution directory
- *npm run-script build --prod* for build command
- *npm run-script start* for start command
- default provider (awscloudformation)
- AWS profile should be the same as what was setup during amplify configure (in my case amplify)

NOTE: You can always re-configure your project by running ```amplify configure project``` when required.

At this point, the Amplify CLI has initialised a new project and a new folder: amplify. The files in this folder hold your project configuration.
```
<amplify-app>
    |_ amplify
      |_ .config
      |_ #current-cloud-backend
      |_ backend
      team-provider-info.json
```

Add missing polyfills for *global* and *process* to polyfills.ts
```javascript
(window as any).global = window;
(window as any).process = {
  env: { DEBUG: undefined },
};
```

When working with underlying aws-js-sdk, the “node” package should be included in types compiler option. Update your src/tsconfig.app.json as follows:
```json
"compilerOptions": {
    "types" : ["node"]
}
```
