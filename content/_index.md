## Front Page Content

Placeholder to write about stuff I learn, mostly IT/Development related.  

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io). It uses the `beautifulhugo` theme which supports content on your front page.

<!-- Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started. -->
