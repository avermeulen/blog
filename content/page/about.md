---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is The Dude. I have the following qualities:

- I rock a great beard (occasionally)
- I'm extremely loyal to my friends
- I love SCUBA diving

I'll add some more once I've managed to direct my thoughts a bit better.

### My history

To be honest, I'm having some trouble remembering right now, so why don't you
just watch [my movie](https://en.wikipedia.org/wiki/The_Big_Lebowski) and it
will answer **all** your questions.
